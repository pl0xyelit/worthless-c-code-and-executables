#include <stdio.h>
int main(){
int num=5;
while(num>0){
num--;
/*
while(num>0){
if(num==3)
break;
printf("%d\n",num);
}

break opreste executarea unei functii, in cazul acesta, cand num este egal cu 3, executarea functiei while se opreste.

*/
if(num==3)
continue;
printf("%d\n",num);
/*continue sare o instructiune. daca n este egal cu 3, se sare peste instructiunea printf() la urmatoarea (daca exista). fiind ultima instructine, while-ul o ia de la inceput cu num--; */
}

return 0;
}
