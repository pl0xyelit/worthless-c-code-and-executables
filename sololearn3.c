#include <stdio.h>
/*
#define PI 3.14
define e pentru a face declaratii constante inaintea inceperii programului cum ar fi variabile constante sau alias-uri
*/
int main() {
/* const double PI=3.14;
const e folosit pentru a declara variabile constante. valorile acestora nu pot fi modificate
*/
int a;
scanf("%d", &a);
/* scanf() cauta input de la tastatura care sa fie de un anume tip specificat, cum ar fi %d sau %c */
printf("You entered: %d", a);
return 0;

}
