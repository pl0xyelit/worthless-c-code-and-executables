#include <stdio.h>
#include <stdlib.h>
int main()
{
        int *ptr;
        int a;
        int i;
	int input;
        printf(" Enter The size of the array \n");
        scanf("%d",&a);
        ptr  = (int *)malloc(a * sizeof(int));
        printf("Assigning the values using index \n");
        for(i = 0; i < a; i++){
	  scanf("%d",&input);
          *(ptr + i) = input;
        }

        /* Printing the array using Index */
        for(i = 0; i < a; i++)
        {
                printf(" ptr[%d] = %d\n",i,*(ptr + i));
        }
	return 0;
}

