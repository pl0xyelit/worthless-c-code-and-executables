#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>

struct frame
{
  uint32_t size_x, size_y; /* specify the size of the array in X & Y pixel count */
  uint32_t* IV_byte_array; /* the data to be written */
};

static uint32_t*
parse_wxh (char* wxh)
{
  if (wxh == NULL) return 0;
  uint32_t* wxh_i = (uint32_t*) malloc (sizeof (uint32_t) * 2);
  uint8_t i = 0;
  wxh_i[0] = 0; wxh_i[1] = 0;
  if (wxh_i == NULL) return 0;
 parse: /* parse */
  while (*wxh >= '0' && *wxh <= '9')
    {
      wxh_i[i] *= 10;
      wxh_i[i] += *wxh - '0';
      wxh++;
    }
  if (*wxh == 'x' && i == 0)
    /* reiterate */
    {wxh++; i++; goto parse;}
  if (wxh_i[0] == 0 || wxh_i[1] == 0)
    /* check it didn't screw up */ {free (wxh_i); return NULL;}
  return wxh_i;
}

static inline void
blank_frame (struct frame* frame, uint32_t colour)
{
  uint32_t iter = 0, goal = frame->size_x * frame->size_y;
  if (frame->IV_byte_array != NULL)
    {
      uint32_t* current_item = frame->IV_byte_array;
      while (iter < goal)
	{
	  *current_item = colour;
	  current_item++;
	  iter++;
	}
    }
}

int
main (int argc, char** argv /* char** env */)
{
  struct frame frame = {0, 0, NULL}; /* initialize struct */
  char ch = 0, *wxh = NULL;
  uint32_t* wxh_i = NULL;
  while ((ch = getopt (argc, argv, "?x:")) != -1)
    {
      switch (ch)
	{
	case 'x':
	  wxh = optarg;
	  break;

	case '?':
	  write (1, "usage: -x WxH\n", 15);
	  return 0;
	  break;

	default:
	  abort ();
	}
    }
  wxh_i = parse_wxh (wxh);
  if (wxh_i == NULL) abort ();
  frame.size_x = wxh_i[0];
  frame.size_y = wxh_i[1];
  free (wxh_i);
  frame.IV_byte_array = (uint32_t*) malloc (sizeof (uint32_t) * frame.size_x * frame.size_y);
  int fb = open ("/dev/fb0", O_RDWR);
  while (1)
    {
      usleep (10000);
      blank_frame (&frame, 0xFFFFFFFFU);
      write (fb, (char*) frame.IV_byte_array, (sizeof (uint32_t) / sizeof (char)) * frame.size_x * frame.size_y);
      ch++;
    }
  return 0;
}

