#include <stdio.h>
int main(){
int j=63;
int *p=NULL;
/*int este tipul de date catre care pointer-ul va indica
  e bine sa initializezi pointer-ii cu valoarea NULL pana ce primesc o valoare
* este folosit pentru a declara un pointer si ar trebui sa apara langa identifier-ul folosit pentru variabila pointer
*/
p=&j;

printf("Adresa lui j este %x\n", &j);
printf("p contine adresa %x\n", p);
printf("Valoarea lui j este %d\n", j);
printf("p arata catre valoarea %d\n", *p);
/*                                     ^ pointerul arata catre adresa datelor stocate in j*/

return 0;
}
