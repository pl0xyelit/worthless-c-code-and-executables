#include <stdio.h>

int main(int argc, char ** argv)
{
    for (int i = 0; i < argc; i++)
    {
        printf(i ? ", '%s'" : "['%s'", argv[i]);
    }
    printf("]\n");
    return 0;
}
